# Dependencies

You will need to install the next dependencies in order to use this code:

1. IPFS
2. ipfsapi
3. PyCryptodome
4. Tkinter

I suggest you to use a virtual environment to install all of them. 

# Structure of the Tkinter parts

The Tkinter parts in this code follow the next structure:

1. The global variables used in that part
2. The creation of the window
3. The variables used in the window
4. General configuration of the window
5. The creation of the widgets and the rest of the code
6. The placement of the widgets
7. The mainloop

# Acknowledgments

* To Gerald Nash for his amazing articles of the blockchain in python.
* To syedrakib because his code served as an inspiration to build the RSA encryption system.
* To everyone who contributed to The Power of Content-addressing guide on IPFS. It helped me a lot to understand how to use IPFS. 
* To Antonino for helping me with my doubts about asymetric encryption.
* There is definetely other people which knowledge and code helped me and that I am forgetting. To all of them an apologize for and a big thanks!